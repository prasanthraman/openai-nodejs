require('dotenv').config();

const {OpenAI}= require('openai')
const fs= require('fs')
const openai = new OpenAI({
  
  apiKey:process.env["OPEN_AI_API_KEY"],

});



async function emojiChatCompletion() {
  try{
    
    const completion = await openai.chat.completions.create({
      messages: [{ role: 'system', content: `You should respond to users with only emojis` },
                 { role: 'user', content: 'How are different types of emotions? ' }],
      model: 'gpt-3.5-turbo', // various model engines
      max_tokens: 25, // Number of characters in response
      temperature: 0.3, //Determines how narrow the response is. Lower the temperature better the results. If 0 uses log_prob
    });
  
    console.log(completion,completion.choices);
  }

  catch(error){
    console.log("Error:",error);
  }
}
async function funnyChatCompletion() {
  try{
    
    const completion = await openai.chat.completions.create({
      messages: [{ role: 'system', content: `You should respond to users with a funny and sarcastic tone` },
                 { role: 'user', content: 'How is the universe created? ' }],
      model: 'gpt-3.5-turbo', // various model engines
      max_tokens: 25, // Number of characters in response
      temperature: 0.3, //Determines how narrow the response is. Lower the temperature better the results. If 0 uses log_prob
    });
  
    console.log(completion,completion.choices);
  }

  catch(error){
    console.log("Error:",error);
  }
}

async function transcription() {
  const transcription = await openai.audio.transcriptions.create({
    file: fs.createReadStream("audio.mp3"),
    model: "whisper-1",
    prompt: ` Ignore the word test in this audio`,
  });

  console.log(transcription,transcription.text);
}

async function translation() {
  const transcription = await openai.audio.translations.create({
    file: fs.createReadStream("audio.mp3"),
    model: "whisper-1",
    
  });

  console.log(transcription,transcription.text);
}

async function embedding() {
  const embedding = await openai.embeddings.create({
    model: "text-embedding-ada-002",
    input: "The quick brown fox jumped over the lazy dog",
  });

  console.log(embedding,embedding.data[0].embedding);
}

async function imageGenerate() {
  const image = await openai.images.generate({ prompt: "A baby tiger in the middle of dark forest at night with mooonlight" });

  console.log(image,image.data);
}

async function imageEdit() {
  const image = await openai.images.edit({
    image: fs.createReadStream("otter.png"),
    mask: fs.createReadStream("mask.png"),
    prompt: "A cute baby sea otter wearing a beret",
  });

  console.log(image,image.data);
}

async function imageVariation() {
  const image = await openai.images.createVariation({
    image: fs.createReadStream("cat.png"),
    //n:1 //number of images,
    //size:512x512 // size of image,
    //response_format: url,b64_json 
  });

  console.log(image,image.data);
}

async function listModels() {
  const list = await openai.models.list();

  for await (const model of list) {
    console.log(model);
  }
}


async function moderation() {
  const moderation = await openai.moderations.create({ input: "I want to kill myself" });

  console.log(moderation,moderation.results[0].categories,moderation.results[0].category_scores);

}
async function listFiles() {
  const list = await openai.files.list();

  for await (const file of list) {
    console.log(file);
  }
}

emojiChatCompletion();
funnyChatCompletion();

//transcription();
//translation();
//embedding();
//imageGenerate();
//imageEdit();
//imageVariation();
//listModels();
//moderation();
//listFiles();